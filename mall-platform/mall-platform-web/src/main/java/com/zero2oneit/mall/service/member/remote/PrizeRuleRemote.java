package com.zero2oneit.mall.service.member.remote;

import com.zero2oneit.mall.common.annotion.OperateLog;
import com.zero2oneit.mall.common.bean.member.PrizeRule;
import com.zero2oneit.mall.common.enums.BusinessType;
import com.zero2oneit.mall.common.query.member.PrizeRecordQueryObject;
import com.zero2oneit.mall.common.query.member.PrizeRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.feign.member.PrizeFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/24 11:32
 */
@RestController
@RequestMapping("/remote/prizeRule")
@CrossOrigin
public class PrizeRuleRemote {

    @Autowired
    private PrizeFeign prizeRuleFeign;

   /**
     * 查询抽奖规则列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody PrizeRuleQueryObject qo){
        return prizeRuleFeign.list(qo);
    }

    /**
     * 查询抽奖记录列表信息
     * @param qo
     * @return
     */
    @PostMapping("/recordList")
    public BoostrapDataGrid recordList(@RequestBody PrizeRecordQueryObject qo){
        return prizeRuleFeign.recordList(qo);
    }

    /**
     * 添加或编辑抽奖规则信息
     * @param prizeRule
     * @return
     */
    @OperateLog(title = "添加或编辑抽奖规则信息", businessType = BusinessType.UPDATE)
    @PostMapping("/addOrEdit")
    public R addOrEdit(PrizeRule prizeRule){
        return prizeRuleFeign.addOrEdit(prizeRule);
    }

}
