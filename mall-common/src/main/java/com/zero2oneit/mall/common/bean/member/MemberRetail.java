package com.zero2oneit.mall.common.bean.member;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-12
 */
@Data
@TableName("member_retail")
public class MemberRetail implements Serializable {

	private static final long serialVersionUID = 1L;

    	/**
	 * 分销ID
	 */
		@TableId
		private Long id;
		/**
	 * 一级比例
	 */
		private BigDecimal oneLevel;
		/**
	 * 二级比例
	 */
		private BigDecimal twoLevel;
		/**
	 * 提现最低金额
	 */
		private BigDecimal money;
	
}
