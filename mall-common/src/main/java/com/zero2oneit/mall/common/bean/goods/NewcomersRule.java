package com.zero2oneit.mall.common.bean.goods;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-06-02
 */
@Data
@TableName("newcomers_rule")
public class NewcomersRule implements Serializable {

	private static final long serialVersionUID = 1L;

    	/**
	 * 自增ID
	 */
		@TableId
		private Long id;
		/**
	 * 专享规则名称
	 */
		private String ruleName;
		/**
	 * 专享限购次数
	 */
		private Integer purchaseLimit;
		/**
	 * 规则状态: 1-开启 2-关闭
	 */
		private Integer ruleStatus;
		/**
	 * 操作人名称
	 */
		private String operatorName;
		/**
	 * 操作时间
	 */
		private Date operatorTime;
	
}
