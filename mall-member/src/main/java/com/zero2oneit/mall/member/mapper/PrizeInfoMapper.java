package com.zero2oneit.mall.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.member.PrizeInfo;
import com.zero2oneit.mall.common.query.member.PrizeInfoQueryObject;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

/**
 * @author Tg
 * @create 2021-05-24
 * @description
 */
@Mapper
public interface PrizeInfoMapper extends BaseMapper<PrizeInfo> {

    int selectTotal(PrizeInfoQueryObject qo);

    List<HashMap<String,Object>> selectRows(PrizeInfoQueryObject qo);
}
